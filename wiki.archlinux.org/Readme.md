# Arch Wiki archive

Page backups from wiki.archlinux.org, for purposes adjacent to eventually making G-refracta/v-refracta manuals.

Arch wiki pages are licensed under the [GFDL3 or later](https://www.gnu.org/copyleft/fdl.html).
