# Pine64 wiki archive

Page backups from wiki.pine64.org, for purposes adjacent to eventually making G-refracta/v-refracta manuals.

This wiki does not actually have a clear license notice on it, but all we really archived here were Valenoern's pages. Valenoern releases these pages under either the CC-BY-SA 4 or the [GFDL3 or later](https://www.gnu.org/copyleft/fdl.html) at your choice.
