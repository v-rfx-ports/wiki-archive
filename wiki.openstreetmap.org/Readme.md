# OpenStreetMap wiki archive

Page backups from wiki.openstreetmap.org, for purposes adjacent to eventually making G-refracta/v-refracta manuals, or zensekai-related manuals.

OpenStreetMap wiki pages are licensed under the [CC-BY-SA 2](https://wiki.openstreetmap.org/wiki/Wiki_content_license).
