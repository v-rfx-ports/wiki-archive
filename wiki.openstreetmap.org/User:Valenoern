<mediawiki xmlns="http://www.mediawiki.org/xml/export-0.11/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.mediawiki.org/xml/export-0.11/ http://www.mediawiki.org/xml/export-0.11.xsd" version="0.11" xml:lang="en">
  <siteinfo>
    <sitename>OpenStreetMap Wiki</sitename>
    <dbname>wiki</dbname>
    <base>https://wiki.openstreetmap.org/wiki/Main_Page</base>
    <generator>MediaWiki 1.39.3</generator>
    <case>first-letter</case>
    <namespaces>
      <namespace key="-2" case="first-letter">Media</namespace>
      <namespace key="-1" case="first-letter">Special</namespace>
      <namespace key="0" case="first-letter" />
      <namespace key="1" case="first-letter">Talk</namespace>
      <namespace key="2" case="first-letter">User</namespace>
      <namespace key="3" case="first-letter">User talk</namespace>
      <namespace key="4" case="first-letter">Wiki</namespace>
      <namespace key="5" case="first-letter">Wiki talk</namespace>
      <namespace key="6" case="first-letter">File</namespace>
      <namespace key="7" case="first-letter">File talk</namespace>
      <namespace key="8" case="first-letter">MediaWiki</namespace>
      <namespace key="9" case="first-letter">MediaWiki talk</namespace>
      <namespace key="10" case="first-letter">Template</namespace>
      <namespace key="11" case="first-letter">Template talk</namespace>
      <namespace key="12" case="first-letter">Help</namespace>
      <namespace key="13" case="first-letter">Help talk</namespace>
      <namespace key="14" case="first-letter">Category</namespace>
      <namespace key="15" case="first-letter">Category talk</namespace>
      <namespace key="120" case="first-letter">Item</namespace>
      <namespace key="121" case="first-letter">Item talk</namespace>
      <namespace key="122" case="first-letter">Property</namespace>
      <namespace key="123" case="first-letter">Property talk</namespace>
      <namespace key="200" case="first-letter">DE</namespace>
      <namespace key="201" case="first-letter">DE talk</namespace>
      <namespace key="202" case="first-letter">FR</namespace>
      <namespace key="203" case="first-letter">FR talk</namespace>
      <namespace key="204" case="first-letter">ES</namespace>
      <namespace key="205" case="first-letter">ES talk</namespace>
      <namespace key="206" case="first-letter">IT</namespace>
      <namespace key="207" case="first-letter">IT talk</namespace>
      <namespace key="208" case="first-letter">NL</namespace>
      <namespace key="209" case="first-letter">NL talk</namespace>
      <namespace key="210" case="first-letter">RU</namespace>
      <namespace key="211" case="first-letter">RU talk</namespace>
      <namespace key="212" case="first-letter">JA</namespace>
      <namespace key="213" case="first-letter">JA talk</namespace>
      <namespace key="710" case="first-letter">TimedText</namespace>
      <namespace key="711" case="first-letter">TimedText talk</namespace>
      <namespace key="828" case="first-letter">Module</namespace>
      <namespace key="829" case="first-letter">Module talk</namespace>
      <namespace key="2300" case="case-sensitive">Gadget</namespace>
      <namespace key="2301" case="case-sensitive">Gadget talk</namespace>
      <namespace key="2302" case="case-sensitive">Gadget definition</namespace>
      <namespace key="2303" case="case-sensitive">Gadget definition talk</namespace>
      <namespace key="3000" case="first-letter">Proposal</namespace>
      <namespace key="3001" case="first-letter">Proposal talk</namespace>
    </namespaces>
  </siteinfo>
  <page>
    <title>User:Valenoern</title>
    <ns>2</ns>
    <id>299605</id>
    <revision>
      <id>2490288</id>
      <parentid>2488632</parentid>
      <timestamp>2023-03-10T07:14:38Z</timestamp>
      <contributor>
        <username>Valenoern</username>
        <id>140966</id>
      </contributor>
      <comment>Valenoern pronunciation</comment>
      <origin>2490288</origin>
      <model>wikitext</model>
      <format>text/x-wiki</format>
      <text bytes="4046" sha1="509m8akj68vfil564ru3pkzvbmejqn6" xml:space="preserve">Hello. I'm Valenoern, developer of the zensekai engine.&lt;br&gt;
I'm a big fan of kaiju media and "Hero Shows", such as [[wikipedia:Ultraman|Ultra Series]], [[wikipedia:Digimon|Digimon]], [[wikipedia:Kamen Rider|Kamen Rider]], etc.

{| class="wikitable"
| [https://codeberg.org/avatars/de8b743392807330e8087ad2baae0960?size=870 avatar image]
|-
| {{IPA|ˈvælɛnʔɝn}}
|}

== zensekai engine ==

The goal of the zensekai engine is to create a modular and extensible "monster game" engine, ideally one that supports several different gameplay genres and even "crossovers" of data from different game settings (referred to as "Spheres").&lt;br&gt;
Notably I am aiming to create AR map gameplay in the vein of [[wikipedia:Ingress (video game)|Ingress]], [[wikipedia:Pokémon Go|Pokémon Go]], and [[wikipedia:Harry Potter: Wizards Unite|Wizards Unite]].

The zensekai engine has been on hiatus for a while, though it is slowly picking back up again. I have a whole bunch of underlying tasks to solve before I can even create the most basic parts of the toolkit, so the project has been through a lot of reboots and stumbling moments. (The project as a whole began in 2015, but as of now it is 2023.) Currently, I am aiming for only extremely simple demos and generally perfecting and consolidating all the tools I use so I can clean up all the debris from previous attempts and have a solid foundation for moving forward.

== portal guide experiment ==

While I am still solving problems with zensekai, I have been trying to create an informal standard for recording portals that appear in AR map games.&lt;br&gt;
(This likely will not be used for zensekai, as I feel like OpenStreetMap itself allows for much richer information about points of interest than Niantic's simple scheme of one set of coordinates + one image.)

My first task has been to try to finish my old desktop map project, "[https://codeberg.org/Valenoern/spiro spiro]", and to find a good mobile application for displaying personal maps.&lt;br&gt;
When I have a good understanding of simply displaying JOSM files, I can then move on to creating a demo which can chop up OSM information into grid squares and begin to analyse it for monster spawn patterns.

== Current tools ==

=== OSM ===

* [[ID|iD]] - editor I commonly use on desktop
* [[Vespucci]] - Android application for viewing/editing maps. supports custom data layers and importing [[JOSM file format|OSM files]] (maybe eventually [https://github.com/MarcusWolschon/osmeditor4android/issues/2150 OSM layers])
* Go Map!! - iOS editor I may stop using because it doesn't really fit my application philosophy of "apps should be an expression of what users want them to do". I'd be happy if Vespucci was ported to iOS
* [[Organic Maps]] - iOS routing application

=== zensekai ===

* [https://en.wikipedia.org/wiki/Inkscape Inkscape] - for most graphics
* [http://www.sbcl.org/ SBCL] / [https://github.com/hellerve/sbcli sbcli] - lisp programming environment
* [https://codeberg.org/vxsh-suite/bopwiki bop] - idea-recording program, like a Tiddlywiki you can read without code. not ready for public use
* Nextcloud - for transferring files like pencil sketches to PC
* archive.org - for backing up Free Software / Free Culture development files long-term. this takes some explanation - I actually use archive.org items as a bit of a soft [https://en.wikipedia.org/wiki/Getting_Things_Done#/media/File:GTDcanonical.png "GTD" system]
* manjaro - linux distro, which is mostly notable for the [https://wiki.archlinux.org/title/Creating_packages Arch packaging system]


== Profiles ==

* [https://codeberg.org/Valenoern codeberg profile] (code [https://en.wikipedia.org/wiki/Forge_(software) forge])
* [https://archive.org/details/@sougonnatakumi?and%5B%5D=creator%3A%22valenoern%22 archive.org page] - old pictures, code backups, WIPs
* [https://www.openstreetmap.org/user/Valenoern OSM profile] - where I like to play "manually find the pokéstop"
* [https://www.openstreetmap.org/node/9557388530 example found portal: Northern Goshawk manhole]'</text>
      <sha1>509m8akj68vfil564ru3pkzvbmejqn6</sha1>
    </revision>
  </page>
</mediawiki>
