# Wikidata archive

Wikibase RDF items from wikidata.org, largely for use in RD's bibliography experiments, but also for anything RDF items could conceivably be used for

Wikidata **items** are licensed under the [CC0](https://creativecommons.org/publicdomain/zero/1.0/) while **text pages** are licensed under the [CC-BY-SA 3](https://creativecommons.org/licenses/by-sa/3.0/).
